const initializeSlider = () => {
  const runInitializeSlider = () => {
    const variant = $('.experiment-visible').hasClass('experiment-test') ? 'test' : 'control';
    $(`.experiment-${variant} .plans`).slick({
      appendDots: `.experiment-${variant} .pricing-nav-mobile`,
      dots: false,
      centerMode: false,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            dots: true,
            initialSlide: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true
          }
        }
      ]
    })
  };
  window.runInitializeSlider = runInitializeSlider;
  runInitializeSlider();
};
window.initializeSlider = initializeSlider;


document.addEventListener('DOMContentLoaded', initializeFAQTogglers)
document.addEventListener('DOMContentLoaded', openSelectedDetailsByHash)

function initializeFAQTogglers() {
  const togglers = document.querySelectorAll('.faq-toggler')
  const details = document.querySelectorAll('.faq-item')

  for (const toggler of togglers) {
    toggler.addEventListener('click', toggleFAQItemsForGroup)
  }

  for (const detailsElement of details) {
    detailsElement.addEventListener('click', checkIfEntireGroupIsToggled)
  }
}

function toggleFAQItemsForGroup(event) {
  const toggled = event.target.dataset.toggled
  const group = event.target.dataset.toggleGroup
  const details = document.querySelectorAll(`.faq-item[data-toggle-group=${group}`)

  // If we are toggled, set toggled to false, and close all the details items. Change text to "Show all"
  // otherwise, set toggled to true, open all the details items. Change text to "Hide all"
  if (toggled === 'true') {
    event.target.innerHTML = 'Show all'
    event.target.dataset.toggled = 'false'
    for (const detailsElement of details) {
      detailsElement.open = false
    }
  } else {
    event.target.innerHTML = 'Hide all'
    event.target.dataset.toggled = 'true'
    for (const detailsElement of details) {
      detailsElement.open = true
    }
  }
}

function checkIfEntireGroupIsToggled(event) {
  // Find closest details element to event.target
  const closestDetails = event.target.closest('.faq-item')
  const group = closestDetails.dataset.toggleGroup
  const details = document.querySelectorAll(`.faq-item[data-toggle-group=${group}`)
  const toggler = document.querySelector(`.faq-toggler[data-toggle-group=${group}`)

  // If event.target.open is true, then we're toggling it closed.
  // In which case, check if every other details element in the group is closed. 
  // If so, make sure the toggle button reflect that.
  if (closestDetails.open) {
    for (const detailsElement of details) {
      // Don't check the closestDetails, since its status will be reversed at time of callback.
      if (detailsElement.open && detailsElement !== closestDetails) {
        return
      }
    }
    toggler.innerHTML = 'Show all'
    toggler.dataset.toggled = 'false'
  }

  // If event.target.open is false, then we're toggling it open.
  // In which case, check if every other details element in the group is open.
  // If so, make sure the toggle button reflect that.
  if (!closestDetails.open) {
    for (const detailsElement of details) {
      // Don't check the closestDetails, since its status will be reversed at time of callback.
      if (!detailsElement.open && detailsElement !== closestDetails) {
        return
      }
    }
    toggler.innerHTML = 'Hide all'
    toggler.dataset.toggled = 'true'
  }
}

function openSelectedDetailsByHash() {
  if (window.location.hash) {
    const hash = window.location.hash
    const details = document.querySelector(hash).closest('.faq-item')
    if (details) {
      details.open = true
    }
  }
}
