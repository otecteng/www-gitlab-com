---
layout: handbook-page-toc
title: "Customer Programs"
description: "Customer Programs creates communication paths using Gainsight to inform, educate, and learn from our customers."
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## What we do

Customer Programs creates communication paths using Gainsight to inform, educate, and learn from our customers.

You can see more information about what we do and our strategy in the following decks:

- [Strategy](https://docs.google.com/presentation/d/1EsCcVqKYL1WkwFkOZDr6TV_DBJMEvrGy3ErPlJLcfPg/edit#slide=id.g29a70c6c35_0_68)
- [Overview](https://docs.google.com/presentation/d/1JFHS-GiMvIZ6oClLRXDN7HSZaSJca123m2LTF-pPEBg/edit#slide=id.gddb21d186e_0_17)

### Program Content

The Customer Programs team creates content for the wider GitLab community.

We are in the process of converting popular email program series into a static location in the GitLab Docs site. See more about this project in the [Convert Emails to Docs epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/73).

- GitLab Docs
  - [Onboarding quick start](https://docs.gitlab.com/ee/administration/get_started.html)
  - [CI/CD quick start](https://docs.gitlab.com/ee/ci/quick_start/)

## Who we support

We support TAM-assigned and digital-only customers, and provide email programs, webinar planning, and survey information to our TAM and other teams within GitLab.

## Programs lifecycle

Programs follow the lifecycle of the customer, from onboarding to enablement to retention.

![Customer Lifecycle](https://lucid.app/publicSegments/view/7e994704-484f-4d32-a083-6c052b07ba66/image.png)

## Programs overview

Programs can be any of the following:

- Email, either single or a series, sent automatically or manually through Gainsight
- Webinar invitations and promotion through Zoom and Gainsight
- Monthly newsletters
- Surveys sent through Gainsight

### Program recipients

Customer programs are primarily sent to our digital-only GitLab Admin contacts. Monthly newsletters are sent to both TAM-assigned and digital-only GitLab Admin contacts.

For our digital-only customers, we require a GitLab Admin contact to be added to the following deals at the time of the opportunity approval submission:

- `New - First Order`
- `New - Connected`
- `Growth`

Learn more about the GitLab Admin contact role requirement [on the Gainsight Go-To-Market Technical Documentation page](https://about.gitlab.com/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/#gitlab-admin-contact-required).

Programs may also be sent to Sold To contacts, for our web-direct digital-only customers.

## GitLab Admin contacts

You may add a GitLab Admin contact at any point in the customer lifecycle.

**To add a contact using Gainsight:**

1. Navigate to the Gainsight Customer360 page.
1. Under **Contacts**, find the contact name to enroll.
1. Click the `...` icon.
1. Click **View Details**.
1. Edit the `GitLab Role` field.
1. Select `GitLab Admin` from the picklist.
1. Click **Save**.

**To add a contact using Salesforce:**

1. Find the contact in Salesforce.
1. Edit the `Role` field.
1. Select `GitLab Admin` from the picklist.
1. Click **Save**.

See our [GitLab Admin contact required page](https://about.gitlab.com/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/#gitlab-admin-contact-required) for more information.

## Available programs

Programs are available to specific customer types.

### Digital customer programs

The programs below are available to Sales Assisted **digital-only customers who have GitLab Admin-assigned contacts**.

| Program                         | Category    | Content  | Recipients                                              |
|---------------------------------|-------------|----------|---------------------------------------------------------|
| Onboarding intro and enrollment   | Onboarding  | [Copy doc](https://docs.google.com/document/d/12RBtKSNWBvW6_SEfczxO2Bp8JeHvNlh8S01TrFOTp50/edit?usp=sharing) | Customer within the last 3 days                         |
| Onboarding series               | Onboarding  | [Copy doc](https://docs.google.com/document/d/12RBtKSNWBvW6_SEfczxO2Bp8JeHvNlh8S01TrFOTp50/edit?usp=sharing) | Customer ~3-15 days                                        |
| Post-Onboarding Survey          | Onboarding  | [Copy doc](https://docs.google.com/document/d/1B3RV2RuUkb3RzuQeNUTDz1BnpZLRwInnb_igm4ra7aw/edit?usp=sharing) | Participants completed onboarding series                |
| Customer Use Case: CI           | Enablement  | [Copy doc](https://docs.google.com/document/d/1otgcT0U4tbZJ5cGkvpzmCG7EKuSF19cR7EoPkTtRMzY/edit?usp=sharing) | Customer ~45 days                                       |
| Customer Use Case: DevSecOps    | Enablement  | [Copy doc](https://docs.google.com/document/d/1Pw66qGELmFbdzh7iFqLfuWt0dfDWTVqYSNl2hYe2jqE/edit?usp=sharing) | Customer ~70 days                                       |
| Customer Use Case: CD           | Enablement  | [Copy doc](https://docs.google.com/document/d/11x7AYtQoSD51jY2ev5MPv2xZ7qDUKwXMEQpDsSUGlVA/edit?usp=sharing) | Customer ~100 days                                       |
| Monthly Newsletters             | Enablement  | [Epic](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/34)     | Sent Monthly on Fridays                                 |
| NPS/CSAT Post-Onboarding Survey | Onboarding  | [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/228)    | Customer since = 60 days                                |
| NPS/CSAT Pre-Renewal Survey     | Retention   | [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/228)    | Next renewal date = 120 days                            |
| Monthly Webinar Invitations     | Enablement  | [Epic](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/37)   | Sent monthly dependent on event date                     |
| Post Churn Survey (SMB Only)             | Retention   | [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/293)    | Opportunity marked Closed-Lost, Active Subscription = 0 |
| CI Usage Trigger Program        | Enablement  | [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/190) | <20% Utilization of CI and not in time-based CI campaign                                       |
| Low License Utilization (SMB Only)    | Retention   | [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/114)    | License Utilization less than 75%, customer since 90-150 days |

The programs below are available to Web Direct **digital-only** customers and sent to the `Latest Sold To` contact.

| Program                         | Category    | Content  | Recipients                                              |
|---------------------------------|-------------|----------|---------------------------------------------------------|
| Onboarding series               | Onboarding  | [Copy doc](https://docs.google.com/document/d/12RBtKSNWBvW6_SEfczxO2Bp8JeHvNlh8S01TrFOTp50/edit?usp=sharing) | Customer ~3-15 days                                        |
| Post-Onboarding Survey          | Onboarding  | [Copy doc](https://docs.google.com/document/d/1B3RV2RuUkb3RzuQeNUTDz1BnpZLRwInnb_igm4ra7aw/edit?usp=sharing) | Participants completed onboarding series                |
| Customer Use Case: CI           | Enablement  | [Copy doc](https://docs.google.com/document/d/1otgcT0U4tbZJ5cGkvpzmCG7EKuSF19cR7EoPkTtRMzY/edit?usp=sharing) | Customer ~45 days                                       |
| Customer Use Case: DevSecOps    | Enablement  | [Copy doc](https://docs.google.com/document/d/1Pw66qGELmFbdzh7iFqLfuWt0dfDWTVqYSNl2hYe2jqE/edit?usp=sharing) | Customer ~70 days                                       |
| Customer Use Case: CD           | Enablement  | [Copy doc](https://docs.google.com/document/d/11x7AYtQoSD51jY2ev5MPv2xZ7qDUKwXMEQpDsSUGlVA/edit?usp=sharing) | Customer ~100 days                                       |
| Monthly Newsletters             | Enablement  | [Epic](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/34)     | Sent Monthly on Fridays                                 |

<br>

### TAM-assigned account programs

The programs below are available to **TAM-assigned accounts** only.

| Program                               | Category   | Content  | Recipients                              |
|---------------------------------------|------------|----------|-----------------------------------------|
| Intro to GitLab CS + Onboarding   | Onboarding | [Copy doc](https://docs.google.com/document/d/1rS1mV_8eKpM49Oh-35sCwmk_5hpV_CQl8S1g-Jc4Xxk/edit?usp=sharing) | When a customer is first assigned a TAM |
| AE <> TAM Introduction  | Onboarding | [Copy doc](https://docs.google.com/document/d/14k1h_f4d51GjwS9HzAJ6iOzebBu4aEgETfSQwUA-MME/edit?usp=sharing) | When a customer is first assigned a TAM |
| Monthly Newsletters*                  | Enablement | [Epic](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/34)    | Sent monthly on Fridays                 |
| NPS/CSAT Post-Onboarding Survey*      |            | [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/228)    | Customer since = 60 days                |
| NPS/CSAT Pre-Renewal Survey*          |            | [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/228)    | Next renewal date = 120 days            |

_* Monthly newsletters and NPS/CSAT surveys require a GitLab Admin contact_<br>

## Create or update a program

You may request new programs, such as an email series, contribute to existing programs, or update programs. We provide issue templates for ease of use.

**To request a new program or update an existing program:**

1. Open a new issue in the [CS Ops Project](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues).

1. Select the appropriate template:
   - CS Programs Request
   - CS Programs Update Request
   - Program Webinar Request

1. Fill out as much of the template as possible. Missing information may cause delays.

CS Programs will assign the request. For urgent issues, or questions about the request, make an `@mention` to a Gainsight administrator.

## Request customer program research with the Research Template

TAMs can use the Customer Programs Research Template to research new program oppportunities for different stages and use cases. This template provides suggestions for information we can use to create valuable programs for our customers.

**To create a Program Research issue**:

1. Navigate to the [CS Ops Issue board](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues).
1. Create a new issue.
1. Select the `TAM Program Research Template` from the dropdown menu.
1. Use the questions in the template to gather as much information as possible for the stage or use case.
1. Assign the template to a TAM or TAM manager for peer review. This will be a multi-step process, as you'll need to ask questions of your customers as well as internal sources.
1. Once TAM peer review is complete, create a new issue [using the appropriate template](#create-or-update-a-program), and relate this issue.

## Create a CTA with a Digital Program playbook

Program emails intended to be sent as a sequence have been added as [playbooks](/handbook/customer-success/tam/gainsight/#ctas) in Gainsight. These need to be sent manually by the TAM. They are set up to facilitate reminders and make this process easy, while we learn how customers want to engage.

See the [Customer Success Playbooks page](https://about.gitlab.com/handbook/customer-success/playbooks/) for a list of current playbooks.

**To use a playbook to send emails**:

1. In Gainsight, create a new CTA in the Customer 360.
1. For `Type`, choose **Digital Journey**.
1. Select the appropriate playbook. For example: `New Customer Digital Onboarding - Self-Managed Email Series`, which will add a checklist for each email in the sequence.
1. Save the CTA.
1. Click the whitespace next to the name of the next email to be sent.
1. Click **Validate Email**. This opens an editor where you can choose contacts and modify the email before sending it.
1. Click **Preview and Proceed** to see the fully populated and formatted email before sending.
1. Send the email, or save as a draft.

## A/B testing for digital customers

We apply [A/B testing](https://hbr.org/2017/06/a-refresher-on-ab-testing) to every digital-only program. The Customer Success Operations team created a rule that puts each customer into either A or B.

See more about how we set up A/B testing on the [issue page](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/230).
